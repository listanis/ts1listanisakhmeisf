package Animals;


import Map.Cell;

import java.util.Random;

/**
 * Represents a Zebra in the simulation, which is a type of Animal.
 */
public class Zebra extends Animal {

    /**
     * Random number generator for determining initial move speed and power.
     */
    Random rand = new Random();

    /**
     * Name of the zebra.
     */
    String name = "Zebra";

    /**
     * Creates a new Zebra at the specified position.
     *
     * @param positionX The X-coordinate of the initial position.
     * @param positionY The Y-coordinate of the initial position.
     * @param map       Reference to the map cells.
     */
    public Zebra(int positionX, int positionY, Cell[][] map) {
        super(50); // Default health for zebra
        this.x = positionX;
        this.y = positionY;
        super.moveSpeed = rand.nextInt(1) + 1; // Random initial move speed
        super.power = rand.nextInt(1) + 1; // Random initial power
        super.name = "Zebra";
        super.health = 50; // Default health for zebra
        super.map = map;
    }

    @Override
    public void run() {
        if(isAlive){
        walk(); // Move the zebra
        }
    }
}
