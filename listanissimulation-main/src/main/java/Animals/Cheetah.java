package Animals;


import Map.Cell;

import java.util.Random;

/**
 * Represents a Cheetah in the simulation, which is a type of Animal.
 */
public class Cheetah extends Animal implements Runnable {

    /**
     * Random number generator for determining initial move speed and power.
     */
    Random rand = new Random();

    /**
     * Endurance of the cheetah.
     * If it becomes 0 or lower, the move speed drops significantly.
     */
    protected int endurance = 100;

    /**
     * Creates a new Cheetah at the specified position.
     *
     * @param positionX The X-coordinate of the initial position.
     * @param positionY The Y-coordinate of the initial position.
     * @param map       Reference to the map cells.
     */
    public Cheetah(int positionX, int positionY, Cell[][] map) {
        super(75); // Default health for cheetah
        super.moveSpeed = rand.nextInt(2) + 2; // Random initial move speed
        super.x = positionX;
        super.y = positionY;
        super.health = 75; // Default health for cheetah
        super.power = rand.nextInt(50) + 50; // Random initial power
        super.name = "Cheetah";
        super.map = map;
    }


    @Override
    public void run() {
        if(dayOfLastEating < time.days)
            daysWithoutFood+=1;
        if(super.isAlive){
        attackIfClose(); // Check for nearby animals to attack
        if (daysWithoutFood >= 1) {
            hunt(); // If the cheetah has not eaten for a day or more, it walks randomly
        } else if (daysWithoutFood != 2) {
            hunt(); // Lions walk when hungry
        } else {
            System.out.println(name + " died from hunger");
            this.dieForever(); // If it has been two days since eating, the cheetah dies permanently
        }
        }
    }
}