package Controllers;

import Map.Time;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class SimulationController {
    @FXML
    private Label welcomeText;

    @FXML
    private Label timeLabel;

    @FXML
    private HBox buttonContainer;

    private Time time;
    private MapController mapController;

    public void setTime(Time time) {
        this.time = time;
    }

    public void setMapController(MapController mapController) {
        this.mapController = mapController;
    }

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }

    @FXML
    public void initialize() {
        timeLabel.setStyle("-fx-text-fill: black;");
        createTimeChangeButtons();
    }

    public void updateTime() {
        if (time != null && timeLabel != null) {
            timeLabel.setText(time.toString());
        }
    }

    private void createTimeChangeButtons() {
        for (int i = 0; i < mapController.chunks.length; i++) {
            Button button = new Button("Chunk " + (i + 1));
            int index = i;
            button.setOnAction(event -> changeChunk(index));
            buttonContainer.getChildren().add(button);
        }
    }

    private void changeChunk(int index) {
        mapController.loadChunk(index);
    }
}
