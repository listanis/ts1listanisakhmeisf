module org.examlpe.com.simulation {
    requires javafx.controls;
    requires javafx.fxml;


    opens Application to javafx.fxml;
    exports Application;
    opens Animals to org.mockito;

}