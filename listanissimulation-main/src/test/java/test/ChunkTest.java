package test;

import Animals.Animal;
import Animals.Lion;
import Map.Cell;
import Map.Chunk;
import Map.Time;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ChunkTest {

    private Cell[][] map;
    private Time time;

    Chunk chunk = new Chunk(0, 0, 3, 3);

    @BeforeEach
    void setUp() {
        int mapWidth = 10;
        int mapHeight = 10;
        map = new Cell[mapWidth][mapHeight];
        for (int i = 0; i < mapWidth; i++) {
            for (int j = 0; j < mapHeight; j++) {
                map[i][j] = Mockito.mock(Cell.class);
            }
        }
        time = Mockito.mock(Time.class);
        Animal.time = time;
    }

    @Test
    void testUpdate() { //mock test
        Lion animal1 = Mockito.mock(Lion.class);
        Lion animal2 = Mockito.mock(Lion.class);

        when(map[1][1].containsAnimal()).thenReturn(true);
        when(map[1][1].getAnimalOnCell()).thenReturn(animal1);

        when(map[9][9].containsAnimal()).thenReturn(true);
        when(map[9][9].getAnimalOnCell()).thenReturn(animal2);

        chunk.update();
        chunk.addAnimal(animal1);
        List<Animal> animals = chunk.getAnimals();
        assertEquals(1, animals.size());
        assertTrue(animals.contains(animal1));
        assertFalse(animals.contains(animal2));
    }

    @Test
    void testContains() {
        assertTrue(chunk.contains(1, 1));
        assertFalse(chunk.contains(9, 9));
    }
}
