package test;

import Animals.Animal;
import Animals.Direction;
import Map.Terrain;
import Map.Time;
import Map.Cell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AnimalTest {

    private Cell[][] map;
    private Time time;

    @BeforeEach
    public void setUp() {
        int mapWidth = 10;
        int mapHeight = 10;
        map = new Cell[mapWidth][mapHeight];
        for (int i = 0; i < mapWidth; i++) {
            for (int j = 0; j < mapHeight; j++) {
                map[i][j] = new Cell(false, Terrain.GRASS, i, j);
            }
        }
        time = new Time("real_time");
        Animal.time = time;
        Animal.map = map;
    }

    @Test
    public void testMove() {
        Cell[][] map = new Cell[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                map[i][j] = Mockito.mock(Cell.class);
            }
        }
        Time time = Mockito.mock(Time.class);

       Animal animal = new Animal(1, "TestAnimal", 10, 5, 5, 100, 100, map, time);

        animal.move(Direction.UP);
        assertEquals(6, animal.getY());
        assertEquals(5, animal.getX());

        animal.move(Direction.RIGHT);
        assertEquals(6, animal.getY());
        assertEquals(6, animal.getX());

        animal.move(Direction.DOWN);
        assertEquals(5, animal.getY());
        assertEquals(6, animal.getX());

        animal.move(Direction.LEFT);
        assertEquals(5, animal.getY());
        assertEquals(5, animal.getX());
    }



    @Test
    public void testEat() {
        Animal predator = new Animal(1, "Predator", 10, 5, 5, 50, 100, map, time);
        map[5][5].addAnimal(predator);

        Animal prey = new Animal(1, "Prey", 5, 5, 6, 0, 50, map, time);
        map[5][6].addAnimal(prey);

        predator.eat(prey);
        assertEquals(100, predator.getHealth());
        assertFalse(map[5][6].containsAnimal());
    }

    @Test
    public void testAttack() { //mock
        Cell[][] map = new Cell[10][10];
        Time time = Mockito.mock(Time.class);
        Cell cell = Mockito.mock(Cell.class);
        when(cell.containsAnimal()).thenReturn(false);
        map[5][5] = cell;
        map[5][6] = cell;

        Animal attacker = new Animal(1, "Attacker", 50, 5, 5, 100, 100, map, time);
        map[5][5].addAnimal(attacker);

        Animal target = Mockito.mock(Animal.class);
        when(target.getHealth()).thenReturn(50); // Health before attack
        when(target.isAlive()).thenReturn(true); // Initially alive

        attacker.attack(target);
        assertEquals(50, target.getHealth());

        when(target.getHealth()).thenReturn(0); // Health after fatal attack
        when(target.isAlive()).thenReturn(false); // Target is no longer alive

        attacker.attack(target);
        assertEquals(0, target.getHealth());

        assertFalse(target.isAlive());
    }

    @Test
    public void testDieForever() {
        Cell[][] map = new Cell[10][10];
        Time time = Mockito.mock(Time.class);
        Cell cell = Mockito.mock(Cell.class);

        when(cell.containsAnimal()).thenReturn(false);
        doNothing().when(cell).addAnimal(any());
        when(cell.getAnimalOnCell()).thenReturn(null);

        map[0][0] = cell;

        Animal animal = new Animal(1, "TestAnimal", 10, 0, 0, 100, 100, map, time);
        map[0][0].addAnimal(animal);

        animal.dieForever();

        assertFalse(animal.isAlive());
        assertEquals(0, animal.getHealth());
        assertNull(map[0][0].getAnimalOnCell());
        assertFalse(map[0][0].containsAnimal());
    }


    @Test
    public void testMoveOutsideMapBoundariesUpRight() { //mock test
        Time time = Mockito.mock(Time.class);

        Animal animal = new Animal(1, "TestAnimal", 10, 0, 0, 100, 100, new Cell[10][10], time);
        animal.map = new Cell[10][10];

        animal.move(Direction.DOWN);
        animal.move(Direction.LEFT);
        assertEquals(0, animal.getX());
        assertEquals(0, animal.getY());
    }

    @Test
    public void testMoveOutsideMapBoundariesDownLeft() {
        Animal animal = new Animal(1, "TestAnimal", 10, 9, 9, 100, 100, map, time);

        animal.move(Direction.UP);
        animal.move(Direction.RIGHT);

        assertEquals(9, animal.getX());
        assertEquals(9, animal.getY());
    }




}



