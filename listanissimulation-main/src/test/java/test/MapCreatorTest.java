package test;

import Controllers.MapCreator;
import Animals.Lion;
import Animals.Cheetah;
import Animals.Rhinoceros;
import Animals.Zebra;
import Animals.Elephant;
import Map.Time;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class MapCreatorTest {

    private MapCreator mapCreator;
    private Time timeMock;

    @BeforeEach
    void setUp() {
        timeMock = Mockito.mock(Time.class);
        mapCreator = new MapCreator(10, 10, 2, 2, 1, 3, 1, timeMock);
    }

    @Test
    void testCreateMap() {
        mapCreator.createMap();
        assertEquals(10, mapCreator.map.length);
        assertEquals(10, mapCreator.map[0].length);
    }

    @Test //mock test
    void testFillMap() {
        mapCreator.createMap();
        HashMap<String, Integer> animalList = new HashMap<>();
        animalList.put("Lions", 2);
        animalList.put("Cheetahs", 2);
        animalList.put("Rhinoceros", 1);
        animalList.put("Zebras", 3);
        animalList.put("Elephants", 1);

        Lion lionMock = Mockito.mock(Lion.class);
        Cheetah cheetahMock = Mockito.mock(Cheetah.class);
        Rhinoceros rhinocerosMock = Mockito.mock(Rhinoceros.class);
        Zebra zebraMock = Mockito.mock(Zebra.class);
        Elephant elephantMock = Mockito.mock(Elephant.class);


        mapCreator.fillMap(animalList);

        assertEquals(2, mapCreator.listOfAmountOfAnimals.get("Lions"));
        assertEquals(2, mapCreator.listOfAmountOfAnimals.get("Cheetahs"));
        assertEquals(1, mapCreator.listOfAmountOfAnimals.get("Rhinoceros"));
        assertEquals(3, mapCreator.listOfAmountOfAnimals.get("Zebras"));
        assertEquals(1, mapCreator.listOfAmountOfAnimals.get("Elephants"));

        int totalAnimals = 0;
        for (int i = 0; i < mapCreator.map.length; i++) {
            for (int j = 0; j < mapCreator.map[0].length; j++) {
                if (mapCreator.map[i][j].containsAnimal()) {
                    totalAnimals++;
                }
            }
        }
        int expectedTotalAnimals = 2 + 2 + 1 + 3 + 1;
        assertEquals(expectedTotalAnimals, totalAnimals);

        int lionsCount = 0, cheetahsCount = 0, rhinocerosCount = 0, zebrasCount = 0, elephantsCount = 0;
        for (int i = 0; i < mapCreator.map.length; i++) {
            for (int j = 0; j < mapCreator.map[0].length; j++) {
                if (mapCreator.map[i][j].containsAnimal()) {
                    String animalType = mapCreator.map[i][j].getAnimalOnCell().getClass().getSimpleName();
                    switch (animalType) {
                        case "Lion" -> lionsCount++;
                        case "Cheetah" -> cheetahsCount++;
                        case "Rhinoceros" -> rhinocerosCount++;
                        case "Zebra" -> zebrasCount++;
                        case "Elephant" -> elephantsCount++;
                    }
                }
            }
        }
        assertEquals(2, lionsCount);
        assertEquals(2, cheetahsCount);
        assertEquals(1, rhinocerosCount);
        assertEquals(3, zebrasCount);
        assertEquals(1, elephantsCount);
    }
}
